package net.viralpatel.java;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateUtil {

	// List of all date formats that we want to parse.
	// Add your own format here.
	private static List<SimpleDateFormat> 
	dateFormats = new ArrayList<SimpleDateFormat>() {
		private static final long serialVersionUID = 1L; 
		{
			add(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"));
			add(new SimpleDateFormat("M/dd/yyyy"));
			add(new SimpleDateFormat("dd.M.yyyy"));
			add(new SimpleDateFormat("M/dd/yyyy hh:mm:ss a"));
			add(new SimpleDateFormat("dd.M.yyyy hh:mm:ss a"));
			add(new SimpleDateFormat("dd.MMM.yyyy"));
			add(new SimpleDateFormat("dd-MMM-yyyy"));
			add(new SimpleDateFormat("dd/M/yyyy"));
		}
	};

	/**
	 * Convert String with various formats into java.util.Date
	 * 
	 * @param input
	 *            Date as a string
	 * @return java.util.Date object if input string is parsed 
	 *          successfully else returns null
	 */
	public static Date convertToDate(String input) {
		Date date = null;
		if(null == input) {
			return null;
		}
		for (SimpleDateFormat format : dateFormats) {
			try {
				format.setLenient(false);
				date = format.parse(input);
			} catch (ParseException e) {
				//Shhh.. try other formats
			}
			if (date != null) {
				break;
			}
		}

		return date;
	}


	/**
	 * Verifica se una stringa � un numero (double o int).
	 * @return -1 se non � una stringa valida,
	 * 0 se � un int, 1 se � un double
	 */
	public static int isDoubleOrInt(String num){
		try{
			Integer.parseInt(num);
			return 0;
			
		} catch (Exception e) {
			try{
				Double.parseDouble(num.replace(',', '.'));
				return 1;
				
			} catch (Exception ex) {
				return -1;
			}
		}
	}

}